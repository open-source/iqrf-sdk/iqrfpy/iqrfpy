"""Generic request messages."""

from .generic import GenericRequest

__all__ = (
    'GenericRequest',
)
