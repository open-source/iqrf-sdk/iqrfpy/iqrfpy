"""Utilities module.

This module contains auxiliary methods for handling and validation of DPA and JSON API responses and parsing of sensor data.
It also contains DPA and Sensor standard constants and methods for conversion of data between various types and formats.
"""
