import unittest
from iqrfpy.enums.peripherals import *


class PeripheralsTestCase(unittest.TestCase):

    def test_contains_ok(self):
        self.assertTrue(EmbedPeripherals.COORDINATOR in EmbedPeripherals)
        self.assertTrue(Standards.SENSOR in Standards)

    def test_contains_unknown(self):
        self.assertFalse(-1 in EmbedPeripherals)
        self.assertFalse(-1 in Standards)

    def test_get_member_ok(self):
        value = EmbedPeripherals.COORDINATOR
        self.assertEqual(
            EmbedPeripherals(value),
            EmbedPeripherals.COORDINATOR
        )
        value = Standards.SENSOR
        self.assertEqual(
            Standards(value),
            Standards.SENSOR
        )

    def test_get_member_unknown(self):
        value = -1
        with self.assertRaises(ValueError):
            EmbedPeripherals(value)
        with self.assertRaises(ValueError):
            Standards(value)

    def test_get_member_different_child(self):
        value = Standards.SENSOR
        with self.assertRaises(ValueError):
            EmbedPeripherals(value)


if __name__ == '__main__':
    unittest.main()
