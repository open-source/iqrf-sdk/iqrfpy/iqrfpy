import unittest
from parameterized import parameterized
from typing import Union

from iqrfpy.utils.dpa import (
    McuTypes,
    TrDTypes,
    TrGTypes,
    ResponseCodes,
    RfBands,
)


class DpaTestCase(unittest.TestCase):

    @parameterized.expand([
        ['OK', ResponseCodes.OK, 'No error'],
        ['ERROR_FAIL', ResponseCodes.ERROR_FAIL, 'General fail'],
        ['ERROR_PCMD', ResponseCodes.ERROR_PCMD, 'Incorrect PCMD'],
        ['ERROR_PNUM', ResponseCodes.ERROR_PNUM, 'Incorrect PNUM or PCMD'],
        ['ERROR_ADDR', ResponseCodes.ERROR_ADDR, 'Incorrect Address'],
        ['ERROR_DATA_LEN', ResponseCodes.ERROR_DATA_LEN, 'Incorrect Data length'],
        ['ERROR_DATA', ResponseCodes.ERROR_DATA, 'Incorrect Data'],
        ['ERROR_HWPID', ResponseCodes.ERROR_HWPID, 'Incorrect HW Profile ID used'],
        ['ERROR_NADR', ResponseCodes.ERROR_NADR, 'Incorrect NADR'],
        ['ERROR_IFACE_CUSTOM_HANDLER', ResponseCodes.ERROR_IFACE_CUSTOM_HANDLER, 'Data from interface consumed by Custom DPA Handler'],
        ['ERROR_MISSING_CUSTOM_DPA_HANDLER', ResponseCodes.ERROR_MISSING_CUSTOM_DPA_HANDLER, 'Custom DPA Handler is missing'],
        ['CONFIRMATION', ResponseCodes.CONFIRMATION, 'DPA confirmation']
    ])
    def test_response_codes_to_str(self, _, value: ResponseCodes, expected: str):
        self.assertEqual(
            str(value),
            expected
        )

    @parameterized.expand([
        ['Invalid negative', -1, 'Invalid DPA response code'],
        ['Invalid larger than byte', 256, 'Invalid DPA response code'],
        ['User error', 34, 'User error code'],
        ['Unknown', 12, 'Unknown DPA response code'],
        ['Reserved general fail', 0x41, 'General fail [reserved]'],
        ['Async general fail', 0x81, 'General fail [async]'],
        ['Async ok', 0x80, 'No error [async]']
    ])
    def test_response_codes_to_string(self, _, value: int, expected: str):
        self.assertEqual(
            ResponseCodes.to_string(value),
            expected
        )

    @parameterized.expand([
        [0],
        [0x40],
        [0x80],
        [0xC0],
    ])
    def test_response_codes_is_ok_response_ok(self, value: int):
        self.assertTrue(ResponseCodes.is_ok_response(value))

    @parameterized.expand([
        [1],
        [0x42],
        [0x83],
        [0xC4],
    ])
    def test_response_codes_is_ok_response_nok(self, value: int):
        self.assertFalse(ResponseCodes.is_ok_response(value))

    @parameterized.expand([
        ['MCU type D', McuTypes.PIC16LF1938, 'D'],
        ['MCU type G', McuTypes.PIC16LF18877, 'G'],
        ['Unknown', 10, '?'],
    ])
    def test_mcu_types_get_mcu_code_str(self, _, value: Union[McuTypes, int], expected: str):
        self.assertEqual(
            McuTypes.get_mcu_code_str(value),
            expected
        )

    @parameterized.expand([
        ['TR-58D-RJ', TrDTypes.TR_58D_RJ, 'TR-58D-RJ'],
        ['TR-72D', TrDTypes.TR_72D, 'TR-72D'],
        ['TR-76D', TrDTypes.TR_76D, 'TR-76D'],
        ['Unknown', 70, 'Unknown'],
    ])
    def test_tr_d_types_get_tr_series_str(self, _, value: Union[TrDTypes, int], expected: str):
        self.assertEqual(
            TrDTypes.get_tr_series_str(value),
            expected
        )

    @parameterized.expand([
        ['TR-72G', TrGTypes.TR_72G, 'TR-72G'],
        ['TR-82G', TrGTypes.TR_82G, 'TR-82G'],
        ['Unknown', 70, 'Unknown'],
    ])
    def test_tr_g_types_to_str(self, _, value: Union[TrGTypes, int], expected: str):
        self.assertEqual(
            TrGTypes.get_tr_series_str(value),
            expected
        )

    @parameterized.expand([
        ['868', RfBands.RF_BAND_868, '868 MHz'],
        ['433', RfBands.RF_BAND_433, '433 MHz'],
        ['916', RfBands.RF_BAND_916, '916 MHz'],
        ['Reserved', RfBands.RF_BAND_RESERVED, 'Reserved']
    ])
    def test_rf_bands_to_str(self, _, value: RfBands, expected: str):
        self.assertEqual(
            str(value),
            expected
        )
