pytest==7.4.0
pytest-cov==4.1.0
pycodestyle==2.11.0
pydocstyle
pylint==2.17.5
parameterized==0.9.0

